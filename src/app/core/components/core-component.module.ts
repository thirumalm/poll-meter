import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { LeftNavComponent } from './left-nav/left-nav.component';
import { MainContainerComponent } from './main-container/main-container.component';
import { FooterComponent } from './footer/footer.component';

import { MaterialModule } from '@shared/components/material/material.module';

@NgModule({
  declarations: [HeaderComponent, LeftNavComponent, MainContainerComponent, FooterComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports:[HeaderComponent, LeftNavComponent, MainContainerComponent, FooterComponent]
})
export class CoreComponentModule { }
